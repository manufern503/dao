<?php 

namespace App\Models;
use App\Entities\User;
use CodeIgniter\Model;

class Producto extends Model{


public function getProductos(){

    	$pa_consultar = 'Call pa_consultar()';
		$query = $this->query($pa_consultar);
	
         $Product = array();

         foreach ($query->getResult() as $row) {
         $ax = new User();
         $ax->setIdProducto($row->id_producto);
         $ax->setNombreproducto($row->nombre_producto);
         $ax->setPrecio($row->precio);
         $ax->setStock($row->stock);
         $ax->setIdProveedor($row->proveedor);
         $ax->setIdMarca($row->marca);
         $ax->setIdCategoria($row->nombres);
         array_push($Product, $ax);
         }
          return $Product;

	     }

	     public function eliminar($id)
	{
		$pa_eliminar = 'CALL pa_eliminar(?)';
		$query = $this->query($pa_eliminar, [$id]);

		if ($query) {
			return true;
		}else{
			return false;
		}
	}

	public function getProveedor()
	{
		$getProveedores = 'CALL getProveedores()';
		$query = $this->query($getProveedores);

		return $query->getResult();
	}
	public function getMarca()
	{
		$getMarcas = 'CALL getMarcas()';
		$query = $this->query($getMarcas);

		return $query->getResult();
	}
	public function getCategoria()
	{
		$getCategorias = 'CALL getCategorias()';
		$query = $this->query($getCategorias);

		return $query->getResult();
	}


	public function insertar($user)
	{
		$pa_insertar = 'CALL pa_insertar(?, ?, ?, ?, ?, ?)';
		$query = $this->query($pa_insertar, [$user->getNombreproducto() ,$user->getPrecio(),$user->getStock() ,$user->getProveedor(),$user->getMarca(),$user->getCategoria()]);

		if ($query){
			return "add";
		}else{
			return "errorA";
		}
	}

	public function getProducto($id)
	{
		$pa_consultarporid = 'CALL pa_consultarporid(?)';
		$query = $this->query($pa_consultarporid, [$id]);

		if ($query) {
			foreach ($query->getResult() as $row){
			
		 $user = new User();
		 $user->setIdProducto($row->id_producto);
         $user->setNombreproducto($row->nombre_producto);
         $user->setPrecio($row->precio);
         $user->setStock($row->stock);
         $user->setIdProveedor($row->id_proveedor);
         $user->setIdMarca($row->id_marca);
         $user->setIdCategoria($row->id_categoria);

					return $user;
				}
		}else{
			return false;
		}
	}


	
}

