<body class="container">
	<form action="<?php echo base_url('/producto_codegniter4/public/Productos/insertar') ?>" method="post">
	<table>
		<tr>
			<td><label for="nombre_producto">Producto</label></td>
			<td><input type="text" name="nombre_producto" id="nombre_producto" class="form-control"></td>
		</tr>
		<tr>
			<td><label for="precio">precio</label></td>
			<td><input type="number" name="precio" id="precio" class="form-control"></td>
		</tr>
		<tr>
			<td><label for="stock">stock</label></td>
			<td><input type="number" name="stock" id="stock" class="form-control"></td>
		</tr>
		<tr>
			<td><label for="proveedor">proveedor</label></td>
			<td>
				<select name="proveedor" id="proveedor" class="form-control">
					<option value="">--Seleccione proveedor--</option>
					<?php foreach ($proveedor as $pro) { ?>
						<option value="<?php echo $pro->id_proveedor ?>"><?php echo $pro->proveedor ?></option>
					<?php } ?>
				</select>
			</td>
		</tr>			
		<tr>
			<td><label for="marca">Marca</label></td>
			<td>
				<select name="marca" id="marca" class="form-control">
					<option value="">--Seleccione proveedor--</option>
					<?php foreach ($marca as $mar) { ?>
						<option value="<?php echo $mar->id_marca ?>"><?php echo $mar->marca ?></option>
					<?php } ?>
				</select>
			</td>
		</tr>		
		<tr>
			<td><label for="categoria">categoria</label></td>
			<td>
				<select name="categoria" id="categoria" class="form-control">
					<option value="">--Seleccione categoria--</option>
					<?php foreach ($categoria as $cat) { ?>
						<option value="<?php echo $cat->id_categoria ?>"><?php echo $cat->nombres ?></option>
					<?php } ?>
				</select>
			</td>
		</tr>			
			
	</table>
	<br>
	<input type="submit" name="Enviar" class="btn btn-primary">
	<br>
</form>
	<table class="table table-dark">
		<thead>
			<tr>
				<th>Producto</th>
				<th>Precio</th>
				<th>Stock</th>
				<th>Proveedor</th>
				<th>Marca</th>
				<th>Categoria</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($productos as $p) { ?>
				<tr>
					<td><?= $p->nombre_producto; ?></td>
					<td><?= $p->precio; ?></td>
					<td><?= $p->stock; ?></td>
					<td><?= $p->proveedor; ?></td>
					<td><?= $p->marca; ?></td>
					<td><?= $p->categoria; ?></td>
					<td><a class="btn btn-danger" onclick="return confirm('Desea realmente eliminar este dato?')" href="/producto_codegniter4/public/Productos/delete/<?php echo $p->id_producto;  ?>">Eliminar</a></td>
					<td><a class="btn btn-info" href="/producto_codegniter4/public/Productos/getDatos/<?php echo $p->id_producto;  ?>">Modificar</a></td>
				</tr>
			<?php } ?>
		</tbody>
	</table>



